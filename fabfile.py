from __future__ import with_statement
from fabric.api import *
from fabric.contrib.console import confirm

code_dir = '/var/www/html'
git_repo = 'git@gitlab.com:Mastergalen/Scenario-Week-1.git'
git_user = 'Mastergalen'

def production():
    env.hosts = ["localuser@localhost:3456"]

def hostname():
    run("hostname")

def deploy():
    def fix_permissions():
        run("sudo chmod -R 777 *")

    with cd(code_dir):
        file_count = int(run("find . -maxdepth 1 ! -name '.*' -type f -printf 'x' | wc -c"))

    with settings(warn_only=True):
        if file_count == 0:
            with settings(prompts={'Username for \'https://gitlab.com\':' : git_user }):
                run("sudo git clone " + git_repo + " %s" % code_dir)
                with cd(code_dir):
                    run("sudo mkdir log")
                    run("sudo touch log/production.log")
                    fix_permissions()

                print("Cloned repository")

            with cd(code_dir):
                run("bundle install --path vendor/cache")
                run("rake db:create RAILS_ENV=production")
                run("rake db:migrate RAILS_ENV=production")
                run("rake db:seed RAILS_ENV=production")

        with cd(code_dir):
            fix_permissions()
            run("git fetch --all")
            run("git reset --hard origin/master")
            run("git pull")

# Dump the DB and store it on a remote server
def backup():
    run("backup perform -t db_backup")

# Set up server
def setup():

    # Install Apache
    def apache():
        print("Installing Apache")
        run("sudo yum -y install httpd")
        run("sudo systemctl start httpd.service")
        run("sudo systemctl enable httpd.service")

    def mysql():
        print("Installing MySQL")
        run("sudo yum install -y mariadb-server mariadb")
        run("sudo systemctl start mariadb")
        run("sudo systemctl enable mariadb.service")

    def ruby():
        print("Installing Ruby on Rails")
        run("sudo yum install -y git-core zlib zlib-devel gcc-c++ patch readline readline-devel libyaml-devel libffi-devel openssl-devel make bzip2 autoconf automake libtool bison curl sqlite-devel")

        run("git clone git://github.com/sstephenson/rbenv.git .rbenv")
        run("echo 'export PATH=\"$HOME/.rbenv/bin:$PATH\"' >> ~/.bashrc")
        run("echo 'export PATH=\"$HOME/.rbenv/bin:$PATH\"' >> ~/.bash_profile")
        run("exec $SHELL")

        run("git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build")
        run("echo 'export PATH=\"$HOME/.rbenv/plugins/ruby-build/bin:$PATH\"' >> ~/.bash_profile")
        run("echo 'export PATH=\"$HOME/.rbenv/plugins/ruby-build/bin:$PATH\"' >> ~/.bashrc")
        run("exec $SHELL")

        run("source ~/.bashrc")
        run("source ~/.bash_profile")

        run("rbenv install -v 2.2.1")
        run("rbenv global 2.2.1")

        run("gem install bundler")
        run("gem install mysql2 -v 0.3.20")

        run("gem install rails -v 4.2.0")

        run("rbenv rehash")

        run("sudo yum -y install epel-release")
        run("sudo yum -y install nodejs")

    def mod_passenger():
        print("Installing mod passenger")
        run("sudo yum install -y epel-release pygpgme curl")
        # Add our el7 YUM repository
        run("sudo curl --fail -sSLo /etc/yum.repos.d/passenger.repo https://oss-binaries.phusionpassenger.com/yum/definitions/el-passenger.repo")
        # Install Passenger + Apache module
        run("sudo yum install -y mod_passenger")
        run("sudo systemctl restart httpd")

    def backup_gem():
        run("gem install backup")
        run("backup generate:model --trigger=db_backup --databases='mysql' --storages='scp' --compressor='gzip' --notifiers='mail'")

        run("gem install whenever")
    
        with cd("~/Backup"):
            run("mkdir config")
            run("wheneverize .")

    with settings(warn_only=True):
        # Install apache
        apache()

        # Install Ruby
        ruby()
        
        # Install MySQL (MariaDB)
        mysql()

        # Install mod_passenger for Apache
        mod_passenger()

        backup_gem()

    print("Installation complete")


