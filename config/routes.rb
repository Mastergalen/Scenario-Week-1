Rails.application.routes.draw do
  devise_for :users
  root 'welcome#index'
  
  namespace :api do
      namespace :v1 do
          resources :lists, only: [:index, :create, :show, :update, :destroy] do
            resources :list_items, only: [:create, :update, :destroy], :path => 'items'
          end
      end
  end

  get '/test', to: 'test#test'
  
end
