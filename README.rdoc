= Group 15

Git Repo for the ToDú app by Team 15.

== Team Members
* Galen Han
* Leah Halaseh
* Istvan Hoffer
* Maddie Whitehall

== Overview
* UCL Indigo and AngularJS for front-end
* Ruby on Rails
* MySQL (MariaDB)
* Passenger and Apache as web server
* Python fabric for automation
* Backup gem for backups