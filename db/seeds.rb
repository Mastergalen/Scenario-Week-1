# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

@user = User.create(email: "test@example.org", password: "password")

@list1 = @user.lists.create()

@list1.list_items.create(content: "Buy some milk")
@list1.list_items.create(content: "Eggs")
@list1.list_items.create(content: "Yoghurt")


@listHomework = @user.lists.create()
@listHomework.list_items.create(content: "Create plan", completed: true)
@listHomework.list_items.create(content: "Finish website")
@listHomework.list_items.create(content: "Write report")