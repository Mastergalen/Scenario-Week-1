class CreateListItemsTable < ActiveRecord::Migration
  def change
    create_table :list_items do |t|
      t.integer  :list_id, null: false
      t.text     :content
      t.boolean  :completed, default: false
      t.timestamps
    end

    add_foreign_key :list_items, :lists
  end
end
