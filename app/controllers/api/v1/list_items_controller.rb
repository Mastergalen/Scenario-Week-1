class  Api::V1::ListItemsController < ApplicationController
	protect_from_forgery :only => [] 

	def create
		if current_user
            @list = current_user.lists.find(params[:list_id])

            @listItem = @list.list_items.create(content: params[:content])
            
            render json: @listItem
        else
            render :json => {
                "success" => false,
                "message" => 'You need to be logged in'
             }
        end
	end

	def update
		if current_user
			@listItem = current_user.list_items.find(params[:id])

			if params[:completed]
				@listItem.completed = (params[:completed] == 'true')
			end

			if params[:content] != nil && params[:content] != ''
				@listItem.content = params[:content]
			end

			@listItem.save

			render json: @listItem
		else
            render :json => {
                "success" => false,
                "message" => 'You need to be logged in'
             }
        end
	end

	def destroy
		if current_user
				if current_user.list_items.where(:id => params[:id]).blank?

		            render :json => {
		                "success" => false,
		                "message" => 'Could not delete non-existent list'
		             }

		        else

		        	@listItem = current_user.list_items.find(params[:id])

				 	@listItem.destroy

				 	render :json => {
		                "success" => true,
		            }
		        end
		else
            render :json => {
                "success" => false,
                "message" => 'You need to be logged in'
             }
        end
	end
end






