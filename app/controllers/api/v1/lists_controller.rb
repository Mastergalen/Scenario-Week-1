   class Api::V1::ListsController < ApplicationController
    protect_from_forgery :except => [:create, :destroy, :update] 
    
          
    def index
        if current_user
            @lists = current_user.lists.includes(:list_items).order(created_at: :desc).all
            
            render json: @lists.as_json(include: :list_items)
        else
            render :json => {
                "success" => false,
                "message" => 'You need to be logged in'
             }
        end
        
    end
    
    # /api/v1/lists/{id}
    def show
        if current_user
            #TODO handle no permissions error
            @list = current_user.lists.includes(:list_items).find(params[:id])
            render json: @list.as_json(include: :list_items)
        else
            render :json => {
                "success" => false,
                "message" => 'You need to be logged in'
             }
        end
         
    end
    
    # /api/v1/lists/{id}
    # DELETE
    def destroy
        #TODO permission check
        if List.where(:id => params[:id]).blank?

            render :json => {
                "success" => false,
                "message" => 'Could not delete non-existent list'
             }

        else
            List.find(params[:id]).destroy
             
            render :json => {
                "success" => true,
            }
    #
        end       
    end

    # /api/v1/lists
    def create
        if current_user
            @list = current_user.lists.create()
            
            render json: @list
        else
            render :json => {
                "success" => false,
                "message" => 'You need to be logged in'
             }
        end

    end
end