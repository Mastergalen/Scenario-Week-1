class List < ActiveRecord::Base
    has_many :list_items, dependent: :destroy, :class_name => "ListItem",
                                               :foreign_key => "list_id"
end
